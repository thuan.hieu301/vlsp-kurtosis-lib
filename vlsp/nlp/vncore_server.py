import time
from pathlib import Path
import os
import requests

from git import Repo
from vncorenlp import VnCoreNLP

from ..utils.timeout import timeout


def connect_to_vncore_server(port: int, out_time: int = 0):
    annotator = None
    with timeout(out_time):
        annotator = VnCoreNLP(address="http://127.0.0.1", port=port)
    return annotator 


class Server:
    """
    Setting up basic VnCoreNLP server 


    """
    def __init__(self):
        """
        Initialize a home directory for necessary server components
        """
        home_dir = str(Path.home())
        project_dir = os.path.join(home_dir, '.vlsp_2020_kurtosis')


        # Create folder and subfolder for project
        if not os.path.exists(project_dir):
            os.mkdir(project_dir)
        print(f'Created a directory at {project_dir}!')

        nlp_tool_path = os.path.join(project_dir, 'nlp_tools')
        if not os.path.exists(nlp_tool_path):
            os.mkdir(nlp_tool_path)

        # Get the NLP tools used for the project
        # Get stopwords file
        stopwords_url = 'https://raw.githubusercontent.com/stopwords/vietnamese-stopwords/master/vietnamese-stopwords-dash.txt'
        
        if not os.path.exists(nlp_tool_path + '/stopwords.txt'):
            r = requests.get(stopwords_url)
            with open(nlp_tool_path + '/stopwords.txt', 'w') as f: 
                f.write(r.text) 

        # Get VnCoreNLP repo
        vncore_path = os.path.join(nlp_tool_path, 'VnCoreNLP')
        if not os.path.exists(vncore_path):
            os.mkdir(vncore_path)
            vncore_repo = 'https://github.com/vncorenlp/VnCoreNLP.git'
            Repo.clone_from(vncore_repo, vncore_path)

    def start(self, port: int = None):
        pass
        

class TextPreprocessing:
    """
    This class contains basic preprocessing function for vietnamese text 

    Basic attributes:
        annotator (vncorenlp.VnCoreNLP): The wrapper for VnCoreNLP of Java
        stopwords (list): A list of vietnamese stopwords 

    Basic functionalities:
        tokenzie (obj): return a list (lists) of tokenized object
        postagging (obj): return a list (lists) of postagged object
    """
    def __init__(self, stopword_path: str = None, vncore_port: int = None):
        """
        The constructor for TextPreprocessing class

        Args: 
            stopword_path: str
                Path to the stop word file
            vncore_port: int
                Port of the currently running VnCoreNLP server
        """
        # Loading stopwords 
        # I'm making an assumption that the stopwords are loaded from the local directory
        if vncore_port == None: 
            raise ValueError('Please insert a port')
        self.annotator = connect_to_vncore_server(vncore_port, 30)
        if self.annotator == None:
            raise Exception('Error intialize the annotator! Check if the server started correctly!')

        self.stopwords = []
        if stopword_path != None: 
            with open(stopword_path) as f: 
                data = f.readlines()
                data = [i.replace('\n', '') for i in data]
            self.stopwords = data
            del data # data is not gonna be used anymore

    def tokenize(self, input_obj):
        """Perform auto-tokenization, can pass a single string or a list of string
        
        Args: 
            input_obj: 
                The object you want to tokenize
        Returns:
            The result after tokenization
        """
        if type(input_obj) == str: 
            return self.annotator.tokenize(input_obj)
        else: 
            return [self.annotator.tokenize(i) for i in input_obj]

    def pos_tagging(self, input_obj):
        """Perform auto-pos-tagging, can pass a single string or a list of string 

        Args: 
            input_obj:
                The object you want to pos tag
        Returns: 
            The pos tagging result
        """
        if type(input_obj) == str: 
            return self.annotator.pos_tag(input_obj)
        else:
            return [self.annotator.pos_tag(i) for i in input_obj]

        
