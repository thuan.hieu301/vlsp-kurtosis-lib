from wordcloud import WordCloud

def gen_wordcloud(input_obj: str, output_file: str = None):
    """ Generate word cloud for a specific text body and (if require) save to file
        Also display the plot
    Args: 
        output_file (str): The output path to save the file
    Returns:
        None
    """
    # Check if list
    text = input_obj
    if type(text) == list:
        print('Detecting list, auto concatenating!')
        text = ' '.join(input_obj)
    elif type(text) == str:
        pass
    else: 
        raise TypeError('Input type should be list or str!')
    
    # Generate WordCloud
    wordcloud = WordCloud().generate(text)

    # Write to file if necessary
    if output_file != None: 
        wordcloud.to_file(output_file)