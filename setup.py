import setuptools

# Require library
INSTALL_REQUIRES = [
    'wordcloud>=1.0',
    'vncorenlp>=1.0',
    'gitpython>=3.1'
]

# Load content from README.md file for long description
with open('README.md', 'r') as f: 
    long_desc = f.read()

# Initialize setup
setuptools.setup (
    name='vlsp_2020',
    version='0.0.1',
    author='Thuan Hieu',
    author_email='thuan.hieu301@gmail.com',
    description='A collection of libraries used for vlsp 2020',
    long_desciption=long_desc,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/thuan.hieu301/vlsp-kurtosis-lib',
    packages=setuptools.find_packages(include=["vlsp", "vlsp.nlp", "vlsp.utils", "vlsp.visualization"]),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent"
    ],
    python_requires='>=3.6',
    install_requires=INSTALL_REQUIRES,
)

